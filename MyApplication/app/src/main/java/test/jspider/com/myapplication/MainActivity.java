package test.jspider.com.myapplication;

import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.TextureView;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity  {
    private int count =0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

   public int plus(int count){
       count ++;
       return count;
   }

    public void add(View viewtest) {
        int number=plus(count);
        TextView textview= (TextView) findViewById(R.id.textView);
        textview.setText(Integer.toString(number));//Convert integer to String
        count++;
         }
    public int sub(int count){
        count --;
        return count;
    }

    public void sub(View viewTest) {
        int number1= sub(count);
        TextView textviewref= (TextView) findViewById(R.id.textView);
        textviewref.setText(Integer.toString(number1));//convert integer to String
        count--;
    }
}
